% Lab 1.4 - Animation
% September 20, 2019

## Do Now

In Snap, which two blocks can be used to allow sprites to communicate with each other? Write your answer in Google Classroom

<br/>
_Hint: what category would these blocks be in?_

::: notes

The blocks are `broadcast` and `receive`

:::

## Costumes

Demo time

::: notes

Be sure to show them:

- sprite rotation toggle buttons
- importing costumes
- "next costume" and "switch costume" blocks
- "broadcast" and "receive" blocks

:::

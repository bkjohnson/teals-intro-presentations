% Lab: 1.1
% September 13, 2019

## Lab 1.1 - Day 1

1. Overview ✔
1. Blocks ✔
1. Scripts ✔

# Review

## Terms

- Block
- Script

::: notes

- **Block** - the things you drag and drop from the palette into the scripting area
- **Script** - you _snap_ blocks together to form scripts

:::

## Opposites

![](./move_10.png)

## Opposites

![](./options.png)

::: notes

With programming, there is usually more than one way to solve the problem

:::

## Lab 1.1 - Day 2

1. Overview ✔
1. Blocks ✔
1. Scripts ✔
1. Reporters
1. Stage Position
1. Drawing
1. Mouse Position
1. Forever...
1. **Kaleidescope**

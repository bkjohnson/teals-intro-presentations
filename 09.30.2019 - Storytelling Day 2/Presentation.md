% Storytelling continued
% September 30, 2019

## Do Now

:::::::::::::: {.columns}
::: {.column width="70%"}

Using these blocks, write a SNAP program where 1 sprite acts as a button and when pressed, it increases the size of another sprite.

Submit on Classroom

:::
::: {.column width="30%"}

![](./do_now_blocks2.png)
:::
::::::::::::::

% Lab: Self-Portrait
% September 06,2019

## Do Now

Take 5 minutes to answer the following:

#### I am \_\_.

# Lab

## Part 1 - Explore

- Find 3 things that SNAP can do
- Discuss with a partner

---

## Part 2 - Express Yourself

- Write a SNAP program that describes you
- Include **Student Experiences Survey** info!

---

## Debrief

- What did you discover?
- What did you like? Dislike?

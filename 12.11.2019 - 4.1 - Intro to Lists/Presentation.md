% Unit 4
% December 11, 2019

## The Solar System
![](./solar_system.png)

## Solar System Discoveries

- 8 Planets
- 200+ moons
- 850,000+ asteroids
- 3,500+ comets
- and so much more

## Group Activity

Write an algorithm for adding new Solar System discoveries to an existing collection of older ones.

It should:

- be complete & detailed
- work for any discovery of any name/size

## Code

What might this algorithm look like as code in SNAP?

## Key Components

What do we need?

## Key Components

What do we need?

- A way to store this entire collection of things

## Key Components

What do we need?

- A way to store this entire collection of things
- A way to access & modify individual parts of the collection

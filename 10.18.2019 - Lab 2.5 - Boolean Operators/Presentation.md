% Lab: 2.5
% October 18, 2019

## Do Now

Write an algorithm for swapping the values in two variables like so:

### Before
- dog => "spot"
- cat => "garfield"

### After
- dog => "garfield"
- cat => "spot"

::: notes

Have them swap their answers with a neighbor and discuss

:::

## Review

![](../10.11.2019\ -\ 2.2\ +\ 2.3/conditionals.png)

::: notes

- What are these blocks called?
- What are the blocks called that go in the slots?
  - What do those blocks "return"?

:::

# Boolean Expressions

## Operators

:::::::::::::: {.columns}
::: {.column width="50%"}

### Relational
- **>**
- **<**
- **=**

### Examples:
- `numDogs > 3`
- `planet = "Mars"`

:::
::: {.column width="50%"}

### Boolean

- **and**
- **or**
- **not**

### Examples
- `7 = 3 and 1 < 2`
- `not 5 > 3`
- `name = "Sam" or numDogs > 3`

:::
::::::::::::::

::: notes

Explain that we have already been using boolean expressions, we just didn't know it yet.
We've been using relational operators, but now we will be looking at three new Boolean operators:

::: 

## Practice

1. `1 > 2 AND 4 < 5`
2. `NOT 3 = 3`
3. `5 < 7 OR 2 > 4`
4. `x := 3; | x > 5 OR x < 10`
5. `x := 4; y := 1; | x > y OR x = y`
6. `x := 5; y := 4; | y = 4 AND (NOT y = x)`
7. `x := 2; y := 5; | x = 5 OR (x > 0 AND y < 0)`

::: notes

Boolean operators allow you to use a special trick called "short-circuiting"

:::

## Truth Tables

| p     | q     | AND   | OR    |
| ----- |:-----:|:-----:|:-----:|
| True  | True  | True  | True  |
| True  | False | False | True  |
| False | True  | False | True  |
| False | False | False | False |


| p     | NOT   |
| ----- |:-----:|
| True  | False |
| False | True  |

## Lab Time

Work on Lab 2.5 - use the **Geometry Cheatsheet** if you need to.

% Unit 4.2 - More lists
% December 13, 2019

## Do Now - Letters of a Word

There is a starter project on Google Classroom.

- Read the script
- Run the script
- Modify it so that the sprite says **every letter in the word**.

## Lists

What do they let us do?

## Lists

What do they let us do?

- Store an unknown number of values

## Lists

What do they let us do?

- Store an unknown number of values
- Access/modify specific items in the list

## Lists

What do they let us do?

- Store an unknown number of values
- Access/modify specific items in the list

What are they good for?

- ~~Absolutely nothin'~~

## Lists

What do they let us do?

- Store an unknown number of values
- Access/modify specific items in the list

What are they good for?

- Storing songs in a playlist
- Grocery list
- etc.

# Demo Time

## Lab: You Talkin' To Me?

Lab is posted on Google Classroom

- Try to get creative with your word lists!
  - 2 or 3 items each is fine to start with
- Don't worry about exact grammar
  - i.e. "a otter" vs "an otter"

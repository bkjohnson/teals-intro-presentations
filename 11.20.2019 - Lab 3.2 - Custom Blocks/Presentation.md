% Lab: 3.2
% November 20, 2019

## Do Now

- Link on Google Classroom
- Make sure you follow the directions in the comments!
- Press `space` key to clear the stage between test runs

## Concepts

- **Abstraction**
- **Generalization**
- **Detail Removal**

# Demo time

## Concepts Revisited

- **Abstraction** (`lay brick`, `draw row`, ...)
- **Generalization** (Not limited to 20x10)
- **Detail Removal** (~~set pen to yellow~~)

::: notes

- Abstraction - Removing the specifics that aren't relevant in a given context.
  - Examples: "Give the dog food" vs. "Give the dog Purina puppy chow"
- Generalization - Combining a group of related concepts or processes into a single category. 
- Detail Removal - Reducing the complexity of an algorithm or process by focusing on the important parts.

:::

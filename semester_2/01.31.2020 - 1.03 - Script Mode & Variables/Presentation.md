% Unit 1.03 - Script Mode & Variables 
% January 31, 2020

# Goal 

After this lesson, you will be able to...

- Define and identify **script**, **print**, **run**, **output**, **variable**
- Write a simple script and run it in the IDE
- Print values out to the console (both composed values and from variables)
- Compare script mode vs. interactive mode
- Know how to store a value into a variable

# Today's Plan

- Do Now
- Lesson
- Lab
- Debrief

# Do Now

:::::::::::::: {.columns}
::: {.column width="50%"}

- Open and save a new project called DoNow103 in the IDE.
- Run a program by clicking the ">" Run button
- Practice typing the following expressions in the editor and running the code.
  - 2 * 3 * 5
  - "abc"
  - "abc" + "bde"

:::
::: {.column width="50%"}
Now try typing the statement below into the file

```
print(2*3*5)
```

:::
::::::::::::::

# Review

- In **Interactive mode**, you use a REPL:

**R**ead
**E**xecute
**P**rint
**L**oop

- The `%` operator in Python is the "modulo" operator which gives you the remainder:
  - 4 % 2 -> 0
  - 3 % 2 -> 1

# Variables

- A name that refers to a value
- In Python, the single "equals" sign is for assignment, not an equality check
  - `name = "Bob"`
  - `pi = 3.15159`

# Variable Names

**The Rules**

- letters, numbers, and some punctuation marks are allowed
  - The first character can't be a number
  - You can't use a keyword (i.e. `for`, `while`)
  - Can be of any length
  - Use common sense names

# Lab Time

# Debrief

- What is different between the script and interactive modes?

# Debrief

- What is different between the script and interactive modes?
- How is variable assignment/declaration different between Snap and Python?

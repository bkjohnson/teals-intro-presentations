#!/usr/bin/env bash

INPUT_FILE=$1

IFS="." read -a FILE_NAME <<< $INPUT_FILE

# Convert text file to ppt binary
pandoc $1 -o $FILE_NAME.pptx

libreoffice --headless --convert-to odp $FILE_NAME.pptx

rm $FILE_NAME.pptx

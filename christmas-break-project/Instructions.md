# Caesar cipher project

## Background

During the war-fueled existence of the Roman Republic, Julius Caesar had to have a secure way of communicating sensitive information.
The state-of-the-art encryption technology at the time was a substitution cipher: taking a plain-text message like `attackatdawn` and _substituting_ each letter of the message with a different character.
Caesar's method was to shift each letter 3 characters to the right, so the previous message would be encoded as `dwwdfndwgdzq`.

If a messenger carrying the encoded message was captured by one of Caesar's enemies, the message might have looked like it was indecipherable gibberish.
In order for the captors to discover the hidden message they would have to have known to shift each character to the _left_ 3 times.


## First things first

Before we start writing any code we need to figure out what the requirements are.  For this project we will want two functions (custom blocks): one for **encrypting** and one for **decrypting**.  Let's imagine how we would want to use these functions:

> "I want to encrypt the phrase '`myfavoriteartistisadele`' with a right shift of 1"

"m" would move right to become "n", "y" would move right to become "z", and so on and so forth for each character in the alphabet.

After I have the encoded message, I can then pass it along in complete view of any snooping eyes and without knowing how to decrypt it, anyone that sees it won't know my secret.
Now let's imagine that my message found its way to the intended recipient who now has what appears to be a bunch of gibberish:

> "I want to decrypt the message `'nzgbwpsjufbsujtujtbefmf`'"

_How_ are they going to decrypt this?

In order for this to work, the recipient and myself must have had some sort of secret meeting prior to this where we agreed upon the "key": one shift to the right for encrypting, and one shift to the left for decrypting.

Since the recipient knows the key to decrypting the message, they could think to themselves:

> "I want to decrypt the message `'nzgbwpsjufbsujtujtbefmf`' _with a left shift of 1_"

Using this method, the "n" would move back to an "m", the "z" back to a "y", until each character has been shifted to reveal the message.


## Automate the boring stuff

Most of us aren't going to find joy in manually taking each letter of a message and substituting a different character a certain number of spaces left or right in the alphabet, so it is a good thing that we can get computers to do that for us!
Before we focus too much on the algorithm for encoding and decoding, let's translate those statements from the previous section into what they might look like as custom blocks in snap:

### For encoding

Generalizing the first statement, we might get:

> "I want to encrypt _a phrase_ with a right shift of _some number_"

In a custom block with arguments that could look like:

![](./encode_signature.png)

When we decide to use this block we could use it like this:

![](./encode_example.png)


### For decoding

We can do something very similar for the decode function:

![](./decode_signature.png)

And using the block would look like this:

![](./decode_example.png)


## What about the algorithm?

In our heads we may have a rough idea of the steps involved when doing this manually, so let's write them down:

**encoding**

```
For each letter in the message:
  shift it to the right by the given number

return the encoded message
```

**decoding**

```
For each letter in the encoded message:
  shift it to the left by the given number

return the decoded message
```
---

But wait!
What happens if we reach a "z" character?
Or what if we have a "w" that we want to shift to the right by 5?
We need to have some way of handling those cases where we reach the end of the alphabet.
Let's see what happens if we represent the alphabet as a circle rather than a line with a fixed starting and ending point:

> A -> B -> C ... Y -> Z -> A -> B ...

"z" shifted to the right 3 times would then become "c" - problem solved!
For decoding we would just do the same thing in the opposite direction - "a" shifted to the left two times would become "y".

## Pseudocode to Snap!

So we know we want to look at each letter in the message, one by one.
If you're having trouble thinking of how to do that in Snap, think back to the "Do Now" from 4.2 where you were tasked with debugging a script so that it would say each letter of a word.

The key here is a definite loop!

![](./definite_loop.png)

The variable `i` will change by 1 each time through the loop, and the loop will stop once `i` has reached the length of the plaintext.

---

**Note**

A "definite loop" is a loop where you can know how many times it will loop without running the code.
In this case, if the plaintext message is "hello" then the loop will run 5 times.

An "indefinite loop" is a loop where you can't determine how many times it will run without actually running the code.
An example of this would be a "repeat until" loop where you loop until you find some value that you're looking for.

---

Looping through the message is helpful, but what we _really_ want out of the loop is to get each character in the message.
We've seen the `letter 1 of _` block, so when the time comes we can try to use that.

### Shift it

So we know how to loop and get each character, but how do we shift it?
When we talk about shifting letters to the left or right we can visualize each letter having a neighbor.
In this example let's pretend that "b" is our letter and we want to shift it to the right 2 times:

> A -> [**B**] -> C -> D -> E -> F -> G -> ...

Shifting it to the right once would give us "C":

> A -> B -> [**C**] -> D -> E -> F -> G -> ...

And shifting it to the right one more time would give us "D":

> A -> B -> C -> [**D**] -> E -> F -> G -> ...

Similarly, if we wanted to left-shift "D" twice, we would get "B".
How do we do this kind of thing programatically?

#### Operating on letters

When we shift leters to the right or left we are performing operations on them.
We're already familiar with certain operations like addition and subtraction, so maybe there is a way of doing a similar thing to letters.

Let's say that the expression `B + 1` means "shift 'B' to the right by 1.
This may look weird at first, but think back to one of the blocks we've already seen that might be helpful.
What if we had some way of representing "B" as a number?

Let's pretend that "A" has the value 1, "B" has the value 2, and the rest of the letters have their own value in increasing number.
`B + 1` becomes `2 + 1`, which is 3.
Since 3 is the value for "C", we've done the shift!

We've already seen a block that can help us represent letters as numbers:

![](./unicode.png){.class width=30% }

While in the loop, we can:

1. Take a specific letter
1. Convert it to unicode
1. Add a number to it for the shift
1. Convert the integer value back into an _encoded_ character

Inside our Snap function, that might look like:

![](./unicode_as_letter.png)

There are a lot of things packed together here, so let's break it apart piece by piece starting from the middle.

**letter `i` of plaintext**

`i` is the variable in the loop that is increasing each time, and `plaintext` is the text argument for the function.
We use this block to get an individual character in a word.

**unicode of [ ]**

We pass the result from the previous block into this block in order to get the unicode value.
Turning a letter into a number will let us do the shift.

**[ ] + `shift`**

We take the result of the previous block (the unicode value for a character) and add a number to it, performing the shift.

**unicode [ ] as letter**

Now that we've performed the shift, all we have to do is turn the number back into a letter!

### Combining the characters together

At this point we know how to:

- loop through each character in the word
- shift the character

But how do we get the shifted character into the right spot in the word?
If this were a list we could use the "replace" block, but we don't have that for text values.

We'll just create a new word and append each encoded letter to it as we go!

To do this we're going to use a feature of Snap that we haven't used before: block variables.

To create a block variable, right click on the top block in a block editor.
You should see an option called "block variables".

![](./block_variable.png)

Click it and you should see a new part of your top block with an arrow next to it:

![](./new_block_variable.png)

Clicking the arrow will give us a default block variable of "a":

![](./block_variable_a.png)

Let's give the variable a more meaningful name.
Since this variable will be used to store our encoded text as we make each shift, let's call it `encoded`.
Clicking the "a" block variable should let you rename it:

![](./rename_block_variable.png)

Now we have an appropriately named block variable:

![](./encoded_block_variable.png)

#### Using the block variable

By default Snap sets all new variables to be 0.
Since the `encoded` variable will be used to build up our encoded text character by character, let's use the "set" block to set `encoded` to be empty.

This next part is the key part of the algorithm.
Inside the loop, we will have to join `encoded` with itself and each of the shifted characters.

##### Coming together

Let's take a look at this example:

![](./join_append.png)

`encoded` is first set to be empty.
We're looping as many times as there are letters in the value of `plaintext`.
The interesting part here is what happens inside the loop.

We are reassigning `encoded` to be whatever it was previously, _joined_ with another character.
In this example we are just appending "a" to `encoded` each time, so if `plaintext` was a message with 3 characters, `encoded` would be "aaa".

#### Back to shifting

Let's try a simple example with the word "cat" shifted twice

**First time through the loop**

- `encoded` is empty
- the letter we are working with is "c"
- "c" shifted to the right twice is "e"
- join `encoded` with itself and "e"

**Second time through the loop**

- `encoded` is "e"
- the letter we are working with is "a"
- "a" shifted to the right twice is "c"
- join `encoded` with itself and "c"

**Third time through the loop**

- `encoded` is "ec"
- the letter we are working with is "t"
- "t" shifted to the right twice is "v"
- join `encoded` with itself and "v"

> `encoded` is "ecv"

After building up `encoded` character by character, we can use the `report` block to return it.

Combining each of the pieces together, we end up with this:

![](./encode.png)


## Decoding it all

By this point we should have a working function for encoding the text.
For decoding, we can essentially do the same thing except reverse the direction of the shift!


## Enhancements

You don't need to find solutions for the following questions - they are simply meant to get you thinking.

### Shifting around a circle

Our solution covers all the main points, but there are still things that could be modified or "improved".
Think back to the idea of the alphabet as a circle - shifting "z" to the right twice would give us "b".
What happens if this shift happens in our Snap code?
What might we do to ensure that the shifted value is what we expect?

### Codebreaking

If you took what you learned from implementing the `encode` function and went ahead and implemented the `decode` function, you'll know that it requires the user to know the shift.
Imagine you intercepted a bit of ciphertext and you want to know what it says but you don't know how many times to shift it.
One possible solution would be to try every possible shift until you get a decoded message that is legible - but that could be quite a bit of work.
Is there another way to find the shift?

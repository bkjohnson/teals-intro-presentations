% Lab: 2.2 cont. & 2.3
% October 11, 2019

## Do Now

1. Use the **Repeat** block to draw a square
2. How would you:
   a. use an additional **Repeat** block to draw 12 squares in a line side by side?
   b. modify the code so the squares form a set of stairs going up?

# Interactivity

## Interactivity

- What are examples of interactive programs?
- How could the previous labs have been more interactive?

::: notes

Mention that computer programs usually have some interactive elements to them.
Up until now, most of our programs haven't had much interactivity.

Examples of interactive computer programs.

- Video games
- Messaging apps (any app, really)

:::

## Ask & answer

![](./ask_and_answer.png)

::: notes

Mention that the "answer" block is special, and its shape indicates that.
It isn't a command like most of the other blocks we've been using.

:::

## One at a time

:::::::::::::: {.columns}
::: {.column width="70%"}

- **answer** can only hold one thing at a time
- What will happen here?

:::
::: {.column width="30%"}

![](./multi_ask.png)
:::
::::::::::::::

## Conditionals

:::::::::::::: {.columns}
::: {.column width="70%"}

- A **conditional** is:
  - A block used to make a choice between executing optional code

:::
::: {.column width="30%"}

![](./conditionals.png)
:::
::::::::::::::

## Conditional Operators

![](./operators.png)

## Lab Time

- Work on the **What Shape is That** _or_ the **Yellow Brick Road** lab.

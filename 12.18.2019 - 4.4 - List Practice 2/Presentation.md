% Unit 4.4 - List Practice 2
% December 18, 2019

## Do Now - Traversing a List

1. Describe what it means to "traverse a list"
1. List 2 different blocks you can use to traverse a list and how you can use them.

## Review

What does it mean to traverse a list, and how do you do it?

## Review

What does it mean to traverse a list, and how do you do it?

- Visit each element sequentially

## Review

Index vs. element

- What's the difference?

## Review

Index vs. element

- What's the difference?

- An index is a _reference_ to the content (element)

# Lab Time

% Lab 1.2 - Scavenger Hunt
% September 16, 2019

## Do Now

On Google Classroom, define these terms (feel free to look them up):

- Tracing through code
- Debugging

## Debugging IRL

![](./bug.jpg)

::: notes

Back in the day when a computer could take up an entire room, computer scientist Grace Hopper
coined the term "debugging" adter a moth was found stuck in the hardware

:::

## SNAP offline

- Download SNAP for working on homework
- Programs can be imported/exported

## Block categories

- Why are block categories useful?

## Block categories

![](./bounce_script_pic.png)

## Block categories

![](./bounce_script_pic_b_w.png)

::: notes

One way that categories can be useful is that they give you something to color code.
Color coded blocks are much easier to read & debug than monochromatic ones.

:::

## Lab 1.2 - Scavenger Hunt

- Worksheet on Google Classroom

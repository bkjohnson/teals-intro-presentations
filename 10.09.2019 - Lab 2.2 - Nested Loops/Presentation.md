% Lab: 2.2
% October 09, 2019

## Do Now

:::::::::::::: {.columns}
::: {.column width="70%"}

Using the starting program in Classroom, reassemble the blocks so Alonzo asks how many zigzags to draw. Then have Alonzo draw zigzags from the center of the stage to the upper right the number of times specfied

:::
::: {.column width="30%"}

![](./zig_zag.png)
:::
::::::::::::::

## Review

- What is a loop?
- Why are loops useful
- What problems can be solved with loops?
- How did your shape-drawing scripts improve once loops were added?

::: notes

- Loop: A type of block that causes other code to run multiple times in succession
- Use: To reduce code redundancy & increase readability when dealing with repetition
- Problems: Drawing polygons, drawing staircases
- Improvements: shorter & more readable - easier to make small changes like alter the size of the shape

:::

## Getting Loopy

- **Problem**: Draw two squares side by side.

## Wrap Up

- How did nested loops make the lab easier?
- What would you need to change to alter:
  - size of the road
  - size of each brick

% Lab: 3.4
% November 25, 2019

## Do Now - Practice using arguments

- Link on Google Classroom
- Add an argument named `rate` to the `gravity` block
- Change the `move` block to move "rate" by dragging the rate variable to the `move` block
- Try passing different rates into the `gravity` custom block

## Reporters

![](./Reporter_from_CN8_at_the_Petco_gas_explosion_20050304.jpg)

## Reporters (in Snap)

![](./reporters.png)

## Reporters (in Snap)

::::::::::::::: {.columns}
:::: {.column width="50%"}
Reporters:

- **return** a **value**
- do _not_ have an effect like a command block would

:::
::: {.column width="50%"}

![](./coin_flip.png)

:::
::::::::::::::
## Predicates - the special reporter

::::::::::::::: {.columns}
:::: {.column width="50%"}

Predicates:

- are **reporters** that always return **true/false**

:::
::: {.column width="50%"}

![](./predicates.png)

:::
::::::::::::::
